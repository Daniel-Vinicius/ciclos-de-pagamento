import React from 'react'

export default props => (
    <footer className="main-footer">
        <strong>
            Copyright &copy; 2020
            <a href="https://github.com/Daniel-Vinicius" target="_blank"> Daniel Vinicius</a>
        </strong>
    </footer>
)