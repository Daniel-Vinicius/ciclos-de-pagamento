import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { getSummary } from "./dashboardActions";
import ContentHeader from "../common/template/contentHeader";
import Content from "../common/template/content";
import ValueBox from "../common/widget/valueBox";
import Row from "../common/layout/row";

class Dashboard extends Component {
  componentWillMount() {
    this.props.getSummary();
  }

  render() {
    const { credit, debt } = this.props.summary;
    return (
      <Row>
        <div>
          <ContentHeader title='Dashboard' small='versão 1.0' />
          <Content>
            <ValueBox
              cols='12 4'
              color='green'
              icon='bank'
              value={`R$ ${credit}`}
              text='Total de Créditos'
            />
            <ValueBox
              cols='12 4'
              color='red'
              icon='credit-card'
              value={`R$ ${debt}`}
              text='Total de Débitos'
            />
            <ValueBox
              cols='12 4'
              color='blue'
              icon='money'
              value={`R$ ${credit - debt}`}
              text='Valor Consolidado'
            />
          </Content>
        </div>
      </Row>
    );
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({ getSummary }, dispatch);

const mapStateToProps = state => ({
  summary: state.dashboard.summary,
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
