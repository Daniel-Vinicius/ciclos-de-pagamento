import React from "react";
import { Router, Route, IndexRoute, Redirect, hashHistory } from "react-router";

// import App from "./app";
import AuthOrApp from "./authOrApp";
import Dashboard from "../dashboard/dashboard";
// import Dashboard2 from "../dashboard2/dashboard2";
import BillingCycle from "../billingCycle/billingCycle";

export default props => (
  <Router history={hashHistory}>

    {/* <Route path='/' component={App}> */}
    <Route path='/' component={AuthOrApp}>
      <IndexRoute component={Dashboard} />
      <Route path='billingCycles' component={BillingCycle} />
    </Route>

    {/* <Route path='/' component={Dashboard} /> com href  */}
    {/* <Route path='/' component={Dashboard2}/> estado sem redux */}

    <Redirect from='*' to='/' />
  </Router>
);
